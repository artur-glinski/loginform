<?php

namespace Sda\Logowanie\User;

class User {
	
	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $pass;

    /**
     * User constructor.
     * @param string $email
     * @param string $pass
     */
    public function __construct($email, $pass) {
		$this->email = $email;
		$this->pass = $pass;
	}

	/**
	 * @return mixed
	 */
	// public function validateEmail() {
	// 	return strpos($this->email, '@');
	// }

	/**
	 * @return bool
	 */
	public function validateUser() {
		if(($handle = fopen(__DIR__ . '/../users/users.csv', 'r')) !== FALSE) {
			while (($row = fgetcsv($handle, 1000, ',')) !== FALSE) {
					if ($this->getEmail() === $row[0] && hash_equals($row[1], crypt($this->getPass(), $row[1]))) {
						echo 'Password verified!';
                        fclose($handle);
						return true;
					}
			}
			fclose($handle);
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getPass() {
		return $this->pass;
	}
}