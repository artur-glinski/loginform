<?php

namespace Sda\Logowanie\User;

class RegisterUser {
	
	/**
 * @var string
 */
    private $email;

    /**
     * @var string
     */
    private $pass;

    public function __construct($email, $pass) {
        $this->email = $email;
        $this->pass = $pass;
    }

	public function loginAlreadyExists() {
		if (($handle = fopen(__DIR__ . '/../users/users.csv', 'r')) !== FALSE) {
			while (($row = fgetcsv($handle, 1000, ',')) !== FALSE) {
				if ($this->getEmail() === $row[0]) {
                    fclose($handle);
					return true;
				}
			}
			fclose($handle);
		}
		return false;
	}

	public function addUser() {
		if (strpos($_POST['login'], '@') !== false && isset($_POST['password']) === true) {
			$hashedPassword = crypt($this->getPass(), uniqid('', true));
			$handle = fopen(__DIR__ . '/../users/users.csv', 'a');
			$line = [];
			$line[] = $this->getEmail();
			$line[] = $hashedPassword;
			fputcsv($handle, $line);
			fclose($handle);
			return true;
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	public function getPass() {
		return $this->pass;
	}
}