<?php

namespace Sda\Logowanie\Controller;

use Sda\Logowanie\User\User;
use Sda\Logowanie\User\RegisterUser;
use Sda\Logowanie\Template\Template;


class Controller {

	public function run() {

		if (array_key_exists('action', $_GET)) {
			$action = $_GET['action'];
		} else {
			$action = 'login';
		}

		//$action = array_key_exists('action', $_GET) ? $_GET['action']: 'login';
			
		switch ($action) {
			case 'login':
				$this->login();
				break;
			case 'register':
				$this->register();
				break;
			case 'registered':
				$this->registered();
				break;
			case 'account':
				$this->account();
				break;
			case 'logout':
				$this->logout();
				break;
			default:
				echo '404 brak strony';
				break;
		}
	}

	private function login() {
		unset($_SESSION['registeredEmail']);
		//$template->
		if(array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {

			$user = new User(htmlspecialchars($_POST['login']), htmlspecialchars($_POST['password']));

			if ($user->validateUser() === false) {
				echo 'Niepoprawny login lub hasło<br>';
			} else {
				$_SESSION['login'] = substr($user->getEmail(), 0, strpos($user->getEmail(), '@'));
				$_SESSION['password'] = $_POST['password'];

				header('Location: index.php?action=account');
			}
		}
		//$template->show();
		echo
		'<!DOCTYPE html>
		<html lang="pl">
		<head>
			<meta charset="UTF-8">
			<title>Logowanie</title>
		</head>
		<body>
			<form action="index.php?action=login" method="POST">
				<label for="login">Login </label><input type="text" name="login" id="login" value=""><br>
				<label for="password">Hasło </label><input type="password" name="password" id="password"><br><br>
				<button>Wyślij!</button>
			</form>
			<div>
				<p>Nie masz konta?</p>
				<a href="index.php?action=register">Zarejestruj się!</a>
			</div>
		</body>
		</html>';
	}

	private function account() {
		if (array_key_exists('login', $_SESSION)) {

			echo 'Witaj ' . $_SESSION['login'] . ', jesteś zalogowany!';
			echo '<br>';

			echo 
			'<div style="float: right;">
					<a href="index.php?action=logout">Wyloguj się.</a>

			</div>';
		} else {
			header('Location: index.php?action=login');
		}
	}

	private function register() {
		if(array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {

			$registerUser = new RegisterUser(htmlspecialchars($_POST['login']), htmlspecialchars($_POST['password']));

			if ($registerUser->loginAlreadyExists() === true) {
			 	echo 'Podany email jest zajęty.';
			 	//header('Location: index.php?action=register');
			} elseif ($registerUser->addUser() === true) {
				$_SESSION['registeredEmail'] = $registerUser->getEmail();
				header('Location: index.php?action=registered');
			} else {
				echo 'Niepoprawny login lub hasło<br>';
			}
		}

		echo
	 	'<form action="index.php?action=register" method="POST">
			<label for="login">Stwórz login </label><input type="text" name="login" id="login" value=""><br>
			<label for="password">Stwórz hasło </label><input type="password" name="password" id="password"><br><br>
			<button>Zarejestruj!</button>
		</form>';
	}

	private function registered() {
		if (array_key_exists('registeredEmail', $_SESSION)) {
			echo 'Pomyślnie zarejestrowano!';
			echo
			'<div>
					<a href="index.php?action=login">Przejdź do strony logowania.</a>
			</div>';
		} else {
			header('Location: index.php?action=register');
		}
	}

	private function logout() {
		session_destroy();
		header('Location: index.php?action=login');
	}
}